package controller;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Album;
import main.Photos;

/**
 * This class is the controller for the user view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class UserController {
    @FXML
    Button openAlbumButton, addAlbumButton, deleteAlbumButton, renameAlbumButton, searchPhotosButton;

    @FXML
    TextField albumField;
    @FXML
    ListView<String> albums;

    @FXML
    Label Username;

    public void initialize() {
        Username.setText("User: " + Photos.getCurrUser().getName());
        listAlbums();
    }

    public void listAlbums() {
        ObservableList<String> albumList = FXCollections.observableArrayList();
        for (Album album : Photos.getCurrUser().getAlbums()) {
            albumList.add(album.getName());
        }
        albums.setItems(albumList);
    }

    public void handleAddAlbumButton(ActionEvent e) {
        String albumName = albumField.getText();
        if (albumName.length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No album name provided");
            alert.setHeaderText("Error Adding album");
            alert.setContentText("An album name must be provided to create a new album.");
            alert.showAndWait();
        } else {
            albumName = albumName.toLowerCase();
            if (Photos.getCurrUser().getAlbum(albumName) != null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: Album name already exists");
                alert.setContentText("That album name already exists.");
                alert.showAndWait();
            } else {
                Photos.getCurrUser().addAlbum(albumName);
                listAlbums();
            }
        }
    }

    public void handleDeleteAlbumButton() {
        String selectedAlbum = albums.getSelectionModel().getSelectedItem();
        if (selectedAlbum == null) {
            if (Photos.getLibrary().getUsers().isEmpty()) {
                // there are no albums
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: No albums to delete");
                alert.setHeaderText("Error Deleting Album");
                alert.setContentText("There are no albums.");
                alert.showAndWait();
            } else {
                // no album selected
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: No album selected");
                alert.setHeaderText("Error Deleting Album");
                alert.setContentText("Please select a album to delete.");
                alert.showAndWait();
            }
        } else {
            Photos.getCurrUser().removeAlbum(selectedAlbum);
        }
        listAlbums();
    }

    public void handleRenameButton() {
        String albumName = albums.getSelectionModel().getSelectedItem();
        String newName = albumField.getText().toLowerCase();
        if (albumName == null) {
            // no album selected, error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No album selected");
            alert.setHeaderText("Error Renaming Album");
            alert.setContentText("Please select an album to rename.");
            alert.showAndWait();
        } else if (newName.isEmpty()) {
            // no new name specified, error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No album name provided");
            alert.setHeaderText("Error Renaming Album");
            alert.setContentText("An album name must be provided to rename an album.");
            alert.showAndWait();
        } else {
            albumName = albumName.toLowerCase();
            albumField.setText(newName);
            Photos.getCurrUser().getAlbum(albumName).setName(newName);
            listAlbums();
        }
    }

    public void handleOpenAlbumButton(ActionEvent event) throws IOException {
        String albumName = albums.getSelectionModel().getSelectedItem();
        if (albumName == null) {
            // error, no album selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No album selected");
            alert.setHeaderText("Error Opening Album");
            alert.setContentText("Please select an album to open.");
            alert.showAndWait();
        } else {
            albumName = albumName.toLowerCase();
            Photos.setCurrAlbum(Photos.getCurrUser().getAlbum(albumName));
            Parent root = FXMLLoader.load(getClass().getResource("/view/Album.fxml"));
            Photos.getStage().setScene(new Scene(root, 480, 750));
        }
    }

    public void handleSearchPhotosButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/SearchPhotos.fxml"));
        Photos.getStage().setScene(new Scene(root, 480, 750));
    }

    public void logout(ActionEvent e) {
        try {
            Photos.getLibrary().writeApp();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
            Parent parent = (Parent) loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import main.Photos;
import main.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * This class is the controller for the admin page view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class AdminPageController {
    @FXML
    ListView<String> users;
    @FXML
    Button deleteUserButton, createUserButton;
    @FXML
    TextField userField;

    public void initialize() {
        listUsers();
    }

    public void listUsers() {
        ObservableList<String> userList = FXCollections.observableArrayList();
        for (User user : Photos.getLibrary().getUsers()) {
            userList.add(user.getName());
        }
        this.users.setItems(userList);
    }

    public void deleteUser(ActionEvent e) {
        String selectedUser = users.getSelectionModel().getSelectedItem();
        if (selectedUser == null) {
            if (Photos.getLibrary().getUsers().isEmpty()) {
                // there are no users
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: No users to delete");
                alert.setHeaderText("Error Deleting User");
                alert.setContentText("There are no users.");
                alert.showAndWait();
            } else {
                // no user selected
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: No user selected");
                alert.setHeaderText("Error Deleting User");
                alert.setContentText("Please select a user to delete.");
                alert.showAndWait();
            }
        } else {
            Photos.getLibrary().deleteUser(selectedUser);
        }
        listUsers();
    }

    public void createUser(ActionEvent e) {
        String userName = userField.getText();
        if (userName.length() == 0) {
            // no username was provided
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No username provided");
            alert.setHeaderText("Error Adding User");
            alert.setContentText("A username must be provided to create a new user.");
            alert.showAndWait();
        } else if (!Photos.getLibrary().addUser(userName.toLowerCase())) {
            // username must already exist
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: Username already exists");
            alert.setHeaderText("Error Adding User");
            alert.setContentText("You must pick a different username.");
            alert.showAndWait();
        }
        userField.setText("");
        listUsers();
    }

    public void logout(ActionEvent e) {
        try {
            Photos.getLibrary().writeApp();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

package controller;

import java.io.IOException;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Photos;
import main.User;

/**
 * This class is the controller for the login view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class LoginController {
    @FXML
    private TextField usernameBox;
    @FXML
    public void handleLoginButton(ActionEvent event) throws IOException {

        String username = usernameBox.getText().toLowerCase();
        List<User> users = Photos.getLibrary().getUsers();
        User user = null;
        // Check for valid username. Unsure how to check for users other than admin
        //need to happen before the code below
        for (User currentUser : users) {
            if (currentUser.getName().equals(username)) {
                user = currentUser;
            }
        }
        if (username.equals("admin") || user != null) {
            FXMLLoader loader;
            Parent parent;

            if (username.equals("admin")) {
                loader = new FXMLLoader(getClass().getResource("/view/AdminPage.fxml"));
                parent = loader.load();
                //AdminController controller = loader.<AdminController>getController(); not created yet
                Scene scene = new Scene(parent);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                //controller.start(users);
                stage.setScene(scene);
                stage.show();
            } else {
                Photos.setCurrUser(user);
                loader = new FXMLLoader(getClass().getResource("/view/UserPage.fxml"));
                parent = loader.load();
                // UserController controller = loader.<UserController>getController(); not created yet
                Scene scene = new Scene(parent);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                //controller.start(user, users);
                stage.setScene(scene);
                stage.show();
            }
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Login Error");
            alert.setHeaderText("User not found.");

            alert.showAndWait();
        }

    }
}
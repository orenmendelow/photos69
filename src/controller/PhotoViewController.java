package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import main.Photos;
import javafx.scene.image.Image;
import java.io.File;
import java.io.IOException;

/**
 * This class is the controller for the photo view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class PhotoViewController {

    @FXML
    ImageView imageView;

    @FXML
    ListView<String> tags;

    @FXML
    Label caption, date, tagTypeLabel, tagValueLabel, newTagLabel;

    @FXML
    Button editCaption, saveCaption, addTagButton, deleteTagButton, confirmAddTag, cancelAddTag, addTagType,
            confirmAddTagType, cancelAddTagType, previousButton, nextButton, backButton;

    @FXML
    TextField inputCaption, tagTypeField, tagValueField, newTagTypeField;

    @FXML
    CheckBox canHaveMultiple;

    public void initialize() {
        File f = new File(Photos.getCurrPhoto().getFilepath());
        Image image = new Image(f.toURI().toString());
        imageView.setImage(image);
        displayTags();
        caption.setText("[Caption] " + Photos.getCurrPhoto().getCaption());
        date.setText("[Date] " + Photos.getCurrPhoto().getDate().getTime());
        hideTagAddFeatures();
        hideAddTagTypeControls();
    }

    private void displayTags() {
        ObservableList<String> tagList = FXCollections.observableArrayList();
        for (String key : Photos.getCurrPhoto().getTags().keySet()) {
            for (String tag : Photos.getCurrPhoto().getTags(key)) {
                tagList.add(key + ": " + tag);
            }
        }
        this.tags.setItems(tagList);
    }

    public void editCaption() {
        editCaption.setVisible(false);
        inputCaption.setText(Photos.getCurrPhoto().getCaption());
        inputCaption.setVisible(true);
        saveCaption.setVisible(true);
    }

    public void saveCaption() {
        String newCaption = inputCaption.getText();
        Photos.getCurrPhoto().setCaption(newCaption);
        caption.setText("[Caption] "+newCaption);
        inputCaption.setVisible(false);
        saveCaption.setVisible(false);
        editCaption.setVisible(true);
    }

    public void addTag() {
        showTagAddFeatures();
    }

    private void showTagAddFeatures() {
        editCaption.setVisible(false);
        addTagButton.setVisible(false);
        deleteTagButton.setVisible(false);
        addTagType.setVisible(false);
        tagTypeLabel.setVisible(true);
        tagTypeField.setVisible(true);
        tagValueLabel.setVisible(true);
        tagValueField.setVisible(true);
        confirmAddTag.setVisible(true);
        cancelAddTag.setVisible(true);
    }

    private void hideTagAddFeatures() {
        editCaption.setVisible(true);
        addTagButton.setVisible(true);
        deleteTagButton.setVisible(true);
        addTagType.setVisible(true);
        tagTypeLabel.setVisible(false);
        tagTypeField.setVisible(false);
        tagValueLabel.setVisible(false);
        tagValueField.setVisible(false);
        confirmAddTag.setVisible(false);
        cancelAddTag.setVisible(false);
    }

    public void confirmAddTag() {
        String tagType = tagTypeField.getText().toLowerCase();
        String tagValue = tagValueField.getText().toLowerCase();
        if (tagType.length() == 0 || tagValue.length() == 0) {
            // raise error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: tag value or tag field missing");
            alert.setHeaderText("Error Adding Tag");
            alert.setContentText("Please enter a tag value and tag field to continue");
            alert.showAndWait();
        } else {
            if (Photos.getCurrPhoto().getTags(tagType) == null) {
                // tagType does not exist, raise error
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: invalid tag type");
                alert.setHeaderText("Error Adding Tag");
                alert.setContentText("Please enter a valid tag type");
                alert.showAndWait();
            } else {
                if (Photos.getCurrPhoto().getTags(tagType).contains(tagValue)) {
                    // tag already exists, raise error
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error: tag already exists");
                    alert.setHeaderText("Error Adding Tag");
                    alert.setContentText("That tag already exists.");
                    alert.showAndWait();
                } else if (!Photos.getCurrPhoto().getTags(tagType).isEmpty() &&
                        !Photos.getCurrPhoto().canHaveMultiple(tagType)){
                    // already has one tag value and can't have more, error
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error: tag can only have one value");
                    alert.setHeaderText("Error Adding Tag");
                    alert.setContentText("That tag type already has a value.");
                    alert.showAndWait();
                }else {
                    Photos.getCurrPhoto().addTag(tagType, tagValue);
                    tagTypeField.setText("");
                    tagValueField.setText("");
                    hideTagAddFeatures();
                    displayTags();
                }
            }
        }
    }

    public void cancelAddTag() {
        tagTypeField.setText("");
        tagValueField.setText("");
        hideTagAddFeatures();
    }

    public void deleteTag() {
        String selectedTag = tags.getSelectionModel().getSelectedItem();
        if (selectedTag == null) {
            // no tag selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No tag selected");
            alert.setHeaderText("Error Deleting Tag");
            alert.setContentText("Please select a tag to delete.");
            alert.showAndWait();
        } else {
            int tagSplitIndex = selectedTag.indexOf(':');
            String tagType = selectedTag.substring(0, tagSplitIndex);
            String tagValue = selectedTag.substring(tagSplitIndex+2);
            Photos.getCurrPhoto().removeTag(tagType, tagValue);
        }
        displayTags();
    }

    public void addTagType() {
        showAddTagTypeControls();
    }

    public void confirmAddTagType() {
        String newTagType = newTagTypeField.getText().toLowerCase();
        if (Photos.getCurrUser().getTagTypes().get(newTagType) != null) {
            // tag type already exists
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: tag type already exists");
            alert.setHeaderText("Error Adding Tag Type");
            alert.setContentText("Tag type already exists.");
            alert.showAndWait();
        } else {
            Photos.getCurrUser().addTagType(newTagType, canHaveMultiple.isSelected());
            hideAddTagTypeControls();
        }
    }

    public void cancelAddTagType() {
        newTagTypeField.setText("");
        canHaveMultiple.setSelected(false);
        hideAddTagTypeControls();
    }

    private void showAddTagTypeControls() {
        newTagLabel.setVisible(true);
        newTagTypeField.setVisible(true);
        confirmAddTagType.setVisible(true);
        cancelAddTagType.setVisible(true);
        canHaveMultiple.setVisible(true);
        editCaption.setVisible(false);
        addTagButton.setVisible(false);
        deleteTagButton.setVisible(false);
        addTagType.setVisible(false);
    }

    private void hideAddTagTypeControls() {
        newTagLabel.setVisible(false);
        newTagTypeField.setVisible(false);
        confirmAddTagType.setVisible(false);
        cancelAddTagType.setVisible(false);
        canHaveMultiple.setVisible(false);
        editCaption.setVisible(true);
        addTagButton.setVisible(true);
        deleteTagButton.setVisible(true);
        addTagType.setVisible(true);
    }

    public void displayPreviousPhoto() {
        int indexOfCurrPhoto = Photos.getCurrAlbum().getPhotos().indexOf(Photos.getCurrPhoto());
        if (indexOfCurrPhoto > 0) {
            Photos.setCurrPhoto(Photos.getCurrAlbum().getPhotos().get(indexOfCurrPhoto-1));
            initialize();
        }
    }

    public void displayNextPhoto() {
        int indexOfCurrPhoto = Photos.getCurrAlbum().getPhotos().indexOf(Photos.getCurrPhoto());
        if (indexOfCurrPhoto < Photos.getCurrAlbum().getPhotos().size()-1) {
            Photos.setCurrPhoto(Photos.getCurrAlbum().getPhotos().get(indexOfCurrPhoto+1));
            initialize();
        }
    }

    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/Album.fxml"));
        Photos.getStage().setScene(new Scene(root, 480, 750));
    }

    public void logout(ActionEvent e) {
        try {
            Photos.getLibrary().writeApp();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
            Parent parent = (Parent) loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

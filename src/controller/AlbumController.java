package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.Album;
import main.Photo;
import main.Photos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import javafx.scene.control.ScrollPane;
import javax.imageio.ImageIO;

/**
 * This class is the controller for the album view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class AlbumController {

    @FXML
    Label AlbumName, earliestDate, latestDate, numPhotos;

    @FXML
    Button changeName, confirmChange, showPhoto, deletePhoto, editCaption, moveToAlbum, copyToAlbum, addPhoto,
            doneEditingCaption, confirmMoveAlbum, cancelMoveAlbum, confirmCopy, backButton;

    @FXML
    TextField newName, captionField;

    @FXML
    ScrollPane scrollPane;

    @FXML
    TilePane tilePane;

    @FXML
    ListView<String> albumList;

    private Button currButton;

    public void initialize() throws IOException {
        hidePhotoButtons();
        hideAlbumList();
        captionField.setVisible(false);
        doneEditingCaption.setVisible(false);
        Album currAlbum = Photos.getCurrAlbum();
        AlbumName.setText(currAlbum.getName());
        newName.setText(currAlbum.getName());
        loadAlbumContents();
    }

    public void loadAlbumContents() throws IOException {
        updateDateRange();
        numPhotos.setText("# of photos: " + Photos.getCurrAlbum().getNumPhotos());
        tilePane.getChildren().clear();
        for (Photo photo : Photos.getCurrAlbum().getPhotos()) {
            Button button = new Button(photo.getCaption());
            button.setOnAction(e -> {
                showPhotoButtons();
                Photos.setCurrPhoto(photo);
                currButton = button;
            });
            Image image = ImageIO.read(new FileInputStream(photo.getFilepath()));
            BufferedImage scaledImage = new BufferedImage(150, 150, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(image, 0, 0, 150, 150, null);
            graphics2D.dispose();
            button.setGraphic(new ImageView(SwingFXUtils.toFXImage(scaledImage,null)));
            tilePane.getChildren().add(button);
        }
        scrollPane.setFitToWidth(true);
        scrollPane.setContent(tilePane);
    }

    public void updateDateRange() {
        if (Photos.getCurrAlbum().getPhotos().isEmpty()) {
            earliestDate.setText("earliestDate");
            latestDate.setText("latestDate");
            return;
        }
        Calendar earliest = Photos.getCurrAlbum().getPhotos().get(0).getDate();
        Calendar latest = Photos.getCurrAlbum().getPhotos().get(0).getDate();
        for (Photo photo : Photos.getCurrAlbum().getPhotos()) {
            Calendar photoDate = photo.getDate();
            if (photoDate.compareTo(earliest) < 0) {
                earliest = photoDate;
            }
            if (photoDate.compareTo(latest) > 0) {
                latest = photoDate;
            }
        }
        earliestDate.setText(earliest.getTime().toString());
        latestDate.setText(latest.getTime().toString());
    }

    public void showPhotoButtons() {
        showPhoto.setVisible(true);
        deletePhoto.setVisible(true);
        editCaption.setVisible(true);
        moveToAlbum.setVisible(true);
        copyToAlbum.setVisible(true);
    }

    public void hidePhotoButtons() {
        deletePhoto.setVisible(false);
        editCaption.setVisible(false);
        moveToAlbum.setVisible(false);
        copyToAlbum.setVisible(false);
        showPhoto.setVisible(false);
        currButton = null;
    }

    public void deletePhoto() throws IOException {
        Photos.getCurrUser().removePhoto(Photos.getCurrAlbum(), Photos.getCurrPhoto());
        hidePhotoButtons();
        loadAlbumContents();
    }

    public void editCaption() {
        hideAllButtons();
        captionField.setText(Photos.getCurrPhoto().getCaption());
        captionField.setVisible(true);
        doneEditingCaption.setVisible(true);
    }

    public void doneEditingCaption() {
        Photos.getCurrPhoto().setCaption(captionField.getText());
        captionField.setVisible(false);
        doneEditingCaption.setVisible(false);
        currButton.setText(captionField.getText());
        scrollPane.setContent(tilePane);
        addPhoto.setVisible(true);
    }

    public void hideAllButtons() {
        showPhoto.setVisible(false);
        deletePhoto.setVisible(false);
        editCaption.setVisible(false);
        moveToAlbum.setVisible(false);
        copyToAlbum.setVisible(false);
        addPhoto.setVisible(false);
    }

    public void addPhoto(ActionEvent e) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose photo from file");
        File file = fileChooser.showOpenDialog(Photos.getStage());
        if (file != null && hasValidFileFormat(file)) {
            Photos.getCurrUser().addPhoto(Photos.getCurrAlbum(), file.getAbsolutePath());
            loadAlbumContents();
        }
    }

    private boolean hasValidFileFormat(File file) {
        String filepath = file.getAbsolutePath();
        String fileType3char = filepath.substring(filepath.length()-3).toLowerCase();
        String fileType4char = filepath.substring(filepath.length()-4).toLowerCase();
        return fileType3char.equals("png") || fileType3char.equals("jpg") || fileType3char.equals("gif") ||
                fileType4char.equals("jpeg");
    }

    public void changeAlbumName(ActionEvent e) {
        confirmChange.setVisible(true);
        newName.setVisible(true);
    }

    public void confirmNameChange(ActionEvent e) {
        String newAlbumName = newName.getText();
        // see if name was left the same
        if (newAlbumName.equals(AlbumName.getText())) {
            confirmChange.setVisible(false);
            newName.setVisible(false);
            return;
        }
        if (newAlbumName.length() > 0) {
            // check that album name does not exist under current user
            for (Album album : Photos.getCurrUser().getAlbums()) {
                if (album.getName().equals(newAlbumName)) {
                    // raise error
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error: Album name already exists");
                    alert.setHeaderText("Error Changing Album Name");
                    alert.setContentText("That album name already exists.");
                    alert.showAndWait();
                    newName.setText(AlbumName.getText());
                    confirmChange.setVisible(false);
                    newName.setVisible(false);
                    return;
                }
            }
            Photos.getCurrAlbum().setName(newAlbumName);
            AlbumName.setText(newAlbumName);
            confirmChange.setVisible(false);
            newName.setVisible(false);
        }
    }

    public void moveToAlbum(ActionEvent e) {
        showAlbumList(e);
        hidePhotoButtons();
    }

    public void cancelMoveAlbum() {
        hideAlbumList();
    }

    public void confirmMoveAlbum(ActionEvent e) throws IOException {
        String selectedAlbum = (String) albumList.getSelectionModel().getSelectedItem();
        if (selectedAlbum == null) {
            // no album selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: No album selected");
            alert.setHeaderText("Error");
            alert.setContentText("Please select an album.");
            alert.showAndWait();
        } else {
            Album movingTo = Photos.getCurrUser().getAlbum(selectedAlbum);
            // check if photo is already in that album
            if (movingTo.getPhotos().contains(Photos.getCurrPhoto())) {
                // photo already in designated album
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: Photo already exists");
                alert.setHeaderText("Error");
                alert.setContentText("Selected album already contains photo.");
                alert.showAndWait();
            }
            else {
                movingTo.addPhoto(Photos.getCurrPhoto());
                // if moving, delete photo from this location. otherwise must be a copy
                if (e.getSource().equals(confirmMoveAlbum)) {
                    Photos.getCurrAlbum().removePhoto(Photos.getCurrPhoto());
                    updateDateRange();
                }
                hideAlbumList();
                loadAlbumContents();
            }
        }
    }

    private void showAlbumList(ActionEvent e) {
        albumList.setVisible(true);
        if (e.getSource().equals(moveToAlbum)) {
            confirmMoveAlbum.setVisible(true);
        } else {
            confirmCopy.setVisible(true);
        }
        cancelMoveAlbum.setVisible(true);
        ObservableList<String> albums = FXCollections.observableArrayList();
        for (Album album : Photos.getCurrUser().getAlbums()) {
            if (!album.getName().equals(Photos.getCurrAlbum().getName())) {
                albums.add(album.getName());
            }
        }
        this.albumList.setItems(albums);
    }

    private void hideAlbumList() {
        albumList.setVisible(false);
        cancelMoveAlbum.setVisible(false);
        confirmMoveAlbum.setVisible(false);
        confirmCopy.setVisible(false);
    }

    public void showPhoto() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/ViewPhotos.fxml"));
        Photos.getStage().setScene(new Scene(root, 480, 750));
    }

    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/UserPage.fxml"));
        Photos.getStage().setScene(new Scene(root, 480, 750));
    }

    public void logout(ActionEvent e) {
        try {
            Photos.getLibrary().writeApp();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

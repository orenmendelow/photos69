package controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import main.Album;
import main.Photo;
import main.Photos;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is the controller for the search page view.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class SearchController {

    @FXML
    Button backButton, dateSearch, tagValueSearch, createAlbum, cancelCreateAlbum, confirmCreateAlbum;

    @FXML
    DatePicker fromDate, toDate;

    @FXML
    TextField queryField, albumNameField;

    @FXML
    ScrollPane scrollPane;

    @FXML
    TilePane tilePane;

    private List<Photo> searchResults = new ArrayList<>();

    public void tagValueSearch() throws IOException {
        hideCreateAlbumControls();
        String query = queryField.getText();
        if (query.isEmpty()) {
            // empty query error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: empty search");
            alert.setHeaderText("Error Performing Search");
            alert.setContentText("Please enter a query to search.");
            alert.showAndWait();
        } else {
            query = query.toLowerCase();
            if (hasValidSyntax(query)) {
                searchResults = Photos.getCurrUser().search(query);
                displaySearchResults();
                if (!searchResults.isEmpty()) {
                    createAlbum.setVisible(true);
                }
            } else {
                // invalid syntax, error
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error: invalid search syntax");
                alert.setHeaderText("Error Performing Search");
                alert.setContentText("Invalid search syntax.");
                alert.showAndWait();
            }
        }
    }

    private void displaySearchResults() throws IOException {
        tilePane.getChildren().clear();
        for (Photo photo : searchResults) {
            Image image = ImageIO.read(new FileInputStream(photo.getFilepath()));
            BufferedImage scaledImage = new BufferedImage(150, 150, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(image, 0, 0, 150, 150, null);
            graphics2D.dispose();
            ImageView imageView = (new ImageView(SwingFXUtils.toFXImage(scaledImage,null)));
            tilePane.getChildren().addAll(imageView);
        }
        scrollPane.setFitToWidth(true);
        scrollPane.setContent(tilePane);
    }

    public boolean hasValidSyntax(String s) {
        // must contain '='
        return (s.indexOf('=') >= 0);
    }

    public void dateSearch() throws IOException {
        hideCreateAlbumControls();
        createAlbum.setVisible(false);
        LocalDate localDate = fromDate.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date fromDate = Date.from(instant);
        localDate = toDate.getValue();
        instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date toDate = Date.from(instant);
        searchResults = Photos.getCurrUser().search(fromDate, toDate);
        if (!searchResults.isEmpty()) {
            createAlbum.setVisible(true);
        }
        displaySearchResults();
    }

    public void createAlbum() {
        showCreateAlbumControls();
    }

    public void confirmCreateAlbum() {
        String newAlbumName = albumNameField.getText();
        if (newAlbumName.isEmpty()) {
            // empty name error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error: no name provided");
            alert.setHeaderText("Error Creating Album");
            alert.setContentText("Please enter a name for your new album.");
            alert.showAndWait();
        } else {
            newAlbumName = newAlbumName.toLowerCase();
            for (Album album : Photos.getCurrUser().getAlbums()) {
                if (album.getName().equals(newAlbumName)) {
                    // duplicate name error
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error: album already exists");
                    alert.setHeaderText("Error Creating Album");
                    alert.setContentText("Please pick a new name for your album.");
                    alert.showAndWait();
                    return;
                }
            }
            Photos.getCurrUser().addAlbum(newAlbumName, searchResults);
            hideCreateAlbumControls();
            createAlbum.setVisible(false);
        }
    }

    public void cancelCreateAlbum() {
        hideCreateAlbumControls();
    }

    private void showCreateAlbumControls() {
        createAlbum.setVisible(false);
        albumNameField.setVisible(true);
        cancelCreateAlbum.setVisible(true);
        confirmCreateAlbum.setVisible(true);
    }

    private void hideCreateAlbumControls() {
        createAlbum.setVisible(true);
        albumNameField.setVisible(false);
        cancelCreateAlbum.setVisible(false);
        confirmCreateAlbum.setVisible(false);
    }

    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/UserPage.fxml"));
        Photos.getStage().setScene(new Scene(root, 480, 750));
    }

    public void logout(ActionEvent e) {
        try {
            Photos.getLibrary().writeApp();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
            Parent parent = (Parent) loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

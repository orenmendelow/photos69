package main;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This class contains all the backend for the Photos application.
 */
public class PhotoLibrary implements Serializable{
    /**
     * This field: serialVersionUID, generates a default id and is added to all methods that implement Serializable.
     */
    static final long serialVersionUID = 1L;

    /**
     * This field: users, is the list of User objects, or users, which are using the application.
     */
    private List<User> users;

    /**
     * This field: storeDir, is the directory in which to store the serialized application data for saving and loading
     * sessions.
     */
    public static final String storeDir = "data";

    /**
     * This field: storeFile, the file in which to write the serialized data, in the storeDir directory.
     */
    public static final String storeFile = "users.data";

    /**
     * This method is the constructor for the Photos object. It initializes the users list.
     */
    public PhotoLibrary() {
        this.users = new ArrayList<>();
    }

    /**
     * This method is a getter function for the users of the library.
     * @return Returns all users who have an account in the application.
     */
    public List<User> getUsers() {
        return this.users;
    }

    /**
     * This method returns a user object given their username.
     * @return Returns the user if exists, null otherwise.
     */
    public User getUser(String username) {
        for (User user : users) {
            if (user.getName().equals(username)) {
                return user;
            }
        }
        return null;
    }

    /**
     * This method saves changes to the disk.
     * @throws IOException
     */
    public void writeApp() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(storeDir + File.separator + storeFile));
        oos.writeObject(this);
    }

    /**
     * This method loads the photo album from disk.
     * @return Returns the PhotoLibrary object.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static PhotoLibrary readApp() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(storeDir + File.separator + storeFile));
        return (PhotoLibrary)ois.readObject();
    }

    /**
     * This method creates the stock, the stock album for that user, and fills the album with stock photos.
     * @return Returns the stock user.
     * @throws InterruptedException
     */
    private User createStockUser() throws InterruptedException {
        User stockUser = new User("stock");
        Photos.setCurrUser(stockUser);
        Album stockAlbum = new Album("stock");
        stockUser.addAlbum(stockAlbum);
        Photos.setCurrAlbum(stockAlbum);
        Photo book = new Photo("data/book.jpeg");
        book.addTag("Location", "Library");
        book.setCaption("this is a book!");
        stockUser.addPhoto(stockAlbum, book);
        TimeUnit.SECONDS.sleep(1);
        stockUser.addPhoto(stockAlbum, "data/knight.jpeg");
        TimeUnit.SECONDS.sleep(1);
        stockUser.addPhoto(stockAlbum, "data/knight2.jpeg");
        TimeUnit.SECONDS.sleep(1);
        Photo prof = new Photo("data/prof.jpeg");
        prof.addTag("Person","Sesh");
        prof.setCaption("the fearless leader");
        prof.addTag("Location", "Rutgers");
        stockUser.addPhoto(stockAlbum, prof);
        TimeUnit.SECONDS.sleep(1);
        Photo rutgers = new Photo("data/rutgers.jpeg");
        rutgers.addTag("Location", "Rutgers");
        rutgers.setCaption("RU RAH RAH");
        stockUser.addPhoto(stockAlbum, rutgers);
        TimeUnit.SECONDS.sleep(1);
        Photo shield = new Photo("data/shield.jpeg");
        shield.setCaption("our proud emblem");
        stockUser.addPhoto(stockAlbum, shield);
        stockUser.addTagType("color", true);
        shield.addTag("color", "Red");
        return stockUser;
    }

    /**
     * This method is used in debugging by printing the contents of the library object to the console.
     */
    public void printContents(){
        for (User user : this.users) {
            System.out.println("user: " + user.getName());
            for (Album album : user.getAlbums()) {
                System.out.println("\t" + "album: " + album.getName() + " (" + album.getNumPhotos() + ")");
                for (Photo photo : album.getPhotos()) {
                    System.out.println("\t\t" + photo.getFilepath() + " | " + photo.getCaption() + " | " + photo.getDate().getTime());
                    for (String tagType : photo.getTags().keySet()) {
                        System.out.print("\t\t\t" + tagType + ": ");
                        for (String tag : photo.getTags(tagType)) {
                            System.out.print(tag + " ");
                        }
                        System.out.println();
                    }
                }
            }
        }
    }

    /**
     * This method is used in debugging by loading the photos from disk and displaying them.
     * @param user is the user for whom to show photos.
     * @throws IOException
     */
    public void showPhotos(User user) throws IOException {
        for (Photo photo : user.getAlbums().get(0).getPhotos()) {
            BufferedImage image = ImageIO.read(new File(photo.getFilepath()));
            ImageIcon imageIcon = new ImageIcon(image);
            JLabel jLabel = new JLabel();
            jLabel.setIcon(imageIcon);
            JFrame editorFrame = new JFrame(photo.getCaption());
            editorFrame.getContentPane().add(jLabel, BorderLayout.CENTER);
            editorFrame.pack();
            editorFrame.setLocationRelativeTo(null);
            editorFrame.setVisible(true);
        }
    }

    /**
     * This method adds a user to the application.
     * @param userName is the username for the new user.
     * @return Returns whether adding the new user was successful or not.
     */
    public boolean addUser(String userName) {
        userName = userName.toLowerCase();
        // are they trying to be admin
        if (userName.equals("admin")) {
            return false;
        }
        // check if user already exists
        for (User user : users) {
            if (user.getName().equals(userName)) {
                return false;
            }
        }
        users.add(new User(userName));
        return true;
    }

    /**
     * This method deletes a user from the application.
     * @param userName is the username of which user to delete from the application.
     */
    public void deleteUser(String userName) {
        for (User user : users) {
            if (user.getName().equals(userName)) {
                users.remove(user);
                return;
            }
        }
    }

    /**
     * This method is used to reset the users.data file by wiping any existing users and saving only the stock user to
     * disk.
     * @throws IOException
     * @throws InterruptedException
     */
    public static void resetNewUsersFile() throws IOException, InterruptedException {
        PhotoLibrary library = new PhotoLibrary();
        library.users.add(library.createStockUser());
        library.writeApp();
    }
}

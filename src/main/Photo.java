package main;

import java.io.Serializable;
import java.util.*;

/**
 * This class is the photo object that stores a photo's filepath, date, tags, and caption.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class Photo implements Serializable {

    /**
     * This field: serialVersionUID, generates a default id and is added to all methods that implement Serializable.
     */
    static final long serialVersionUID = 1L;

    /**
     * This field: filepath, the location of the photo file on the disk.
     */
    private String filepath;

    /**
     * This field: date, the date-time at which the photo was added to the library.
     */
    private Calendar date;

    /**
     * This field: tags, the tags and their types that are placed on the photo.
     */
    private Map<String, Tag> tags;

    /**
     * This field: caption, the caption for the photo.
     */
    private String caption;

    /**
     * This method is the constructor for the Photo object.
     * @param filepath The filepath where the photo is located on the disk.
     */
    public Photo(String filepath) {
        this.filepath = filepath;
        this.date = Calendar.getInstance();
        this.date.set(Calendar.MILLISECOND,0);
        this.tags = new HashMap<>();
        this.caption = "";
        Map<String, Tag> userTagTypes = Photos.getCurrUser().getTagTypes();
        for (String key: userTagTypes.keySet()) {
            this.addTagType(key, userTagTypes.get(key).isCanHaveMultiple());
        }
    }

    /**
     * This method is a getter function for the photo's filepath.
     * @return Returns the photo's filepath.
     */
    public String getFilepath() {
        return this.filepath;
    }

    /**
     * This method is a getter function for the photo's date.
     * @return Returns the photo's date.
     */
    public Calendar getDate() {
        return this.date;
    }

    /**
     * This method is a getter function for the photo's caption.
     * @return Returns the photo's caption.
     */
    public String getCaption() {
        return this.caption;
    }

    /**
     * This method is a getter function for the photo's tags.
     * @return Returns the photo's tags.
     */
    public Map<String, Tag> getTags() {
        return tags;
    }

    /**
     * This method returns a list of the tags on the photo under a specific tag type.
     * @param tagType is the type of tag for which all values are wanted.
     * @return Returns all tags under that specified tag type.
     */
    public List<String> getTags(String tagType) {
        List<String> tagStrings = new ArrayList<>(tags.get(tagType).getTags());
        return tagStrings;
    }

    /**
     * This method adds a tag to the photo.
     * @param tagType is the key under which to place the tag. For example, "location."
     * @param tag is the tag to be placed on the photo. For example, "rutgers."
     * @return Returns whether adding the tag was successful.
     */
    public boolean addTag(String tagType, String tag) {
        tagType = tagType.toLowerCase();
        if (tags.get(tagType) != null) {
            if (!tags.get(tagType).getTags().contains(tag)) {
                tags.get(tagType).addTag(tag.toLowerCase());
                return true;
            }
        }
        return false;
    }

    /**
     * This method removes a tag given a tag type and tag value
     * @param tagType is the type of tag the tag to remove is
     * @param tag is the tag value to remove
     */
    public void removeTag(String tagType, String tag) {
        tags.get(tagType).getTags().remove(tag);
    }

    /**
     * This method is to add a tag type to the photo.
     * @param tagType is the tag type to be added.
     * @param canHaveMultiple whether there can be multiple of this type of tag. For example, location can not, but
     *                        person can since multiple people can be in one photo.
     * @return Returns whether adding the tagType was successful.
     */
    public boolean addTagType(String tagType, boolean canHaveMultiple) {
        tagType = tagType.toLowerCase();
        if (tags.get(tagType) == null) {
            tags.put(tagType, new Tag(canHaveMultiple));
            return true;
        }
        return false;
    }

    /**
     * This method returns whether the specified tag type can contain multiple tags.
     * @param tagType is the type of tag being queried.
     * @return Return whether the specified tag type can contain multiple tags.
     */
    public boolean canHaveMultiple(String tagType) {
        if (this.tags.get(tagType) != null) {
            return this.tags.get(tagType).isCanHaveMultiple();
        }
        return false;
    }

    /**
     * This method sets the caption for the photo.
     * @param caption the caption to place on the photo.
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }
}

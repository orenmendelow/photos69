package main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;

/**
 * This class is the driver for the Photos application. It contains the PhotoLibrary object that is used to maintain
 * the library and its contents.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class Photos extends Application {

    /**
     * This field: PhotoLibrary, is the library object which contains all users of the application.
     */
    private static PhotoLibrary library = new PhotoLibrary();

    /**
     * This field: currUser, keeps track of which user is currently logged in.
     */
    private static User currUser;

    /**
     * This field: currAlbum, keeps track of which album we're viewing/ editing.
     */
    private static Album currAlbum;

    /**
     * This field: currPhoto keeps track of which photo we're viewing/ editing.
     */
    private static Photo currPhoto;

    /**
     * This field: stage, is the stage on which we set our JavaFX application.
     */
    private static Stage stage;

    /**
     * This method allows objects outside the scope access the library field.
     * @return Returns the photos library.
     */
    public static PhotoLibrary getLibrary() {
        return library;
    }

    /**
     * This method is a getter for currUser.
     * @return Returns current user.
     */
    public static User getCurrUser() {
        return currUser;
    }

    /**
     * This method is a getter for currAlbum.
     * @return Returns current Album.
     */
    public static Album getCurrAlbum() {
        return currAlbum;
    }

    /**
     * This method is a getter for currPhoto.
     * @return Returns current photo.
     */
    public static Photo getCurrPhoto() {
        return currPhoto;
    }

    /**
     * This method is a setter for currPhoto
     * @param photo is the photo we're currently viewing/ editing.
     */
    public static void setCurrPhoto(Photo photo) {
        currPhoto = photo;
    }

    /**
     * This method is a getter for stage.
     * @return Returns stage.
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * This method is a setter for currUser.
     * @param currUser is the user we're currently logged in as.
     */
    public static void setCurrUser(User currUser) {
        Photos.currUser = currUser;
    }

    /**
     * This method is a setter for currAlbum.
     * @param currAlbum is the album we're currently viewing/ editing.
     */
    public static void setCurrAlbum(Album currAlbum) {
        Photos.currAlbum = currAlbum;
    }

    /**
     * This method is an override method for the JavaFX start(), which is called on application startup. We use the
     * override in order to specify the window size, a title for the window, and which page of our app is the homepage.
     * @param primaryStage is the stage on which the application intends to run.
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/view/LoginPage.fxml"));
        primaryStage.setTitle("Photos");
        primaryStage.setScene(new Scene(root, 480, 750));
        primaryStage.setResizable(false);
        stage = primaryStage;
        primaryStage.show();
    }

    /**
     * This method is an override method for when the application is closed. We use the override to ensure the changes
     * get saved to disk when the application is closed.
     * @throws IOException
     */
    @Override
    public void stop() throws IOException {
        library.writeApp();
    }

    /**
     * This method is the main method which runs the program.
     * @param args is not used.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        // ** use the following command to reset the application to just stock user **
        //PhotoLibrary.resetNewUsersFile();
        library = PhotoLibrary.readApp();
        //library.printContents();

        launch(args);
    }
}

package main;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is the Album object that stores the photos.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class Album implements Serializable {

    /**
     * This field: serialVersionUID, generates a default id and is added to all methods that implement Serializable.
     */
    static final long serialVersionUID = 1L;

    /**
     * This field: name, is the name of the album.
     */
    private String name;

    /**
     * This field: numPhotos, is the number of photos in the album.
     */
    private int numPhotos;

    /**
     * This field: photos, is the list of photos contained within the album.
     */
    private List<Photo> photos;

    /**
     * This method is a constructor for the Album object.
     * @param name is the name to place on the album.
     */
    public Album(String name) {
        this.name = name.toLowerCase();
        this.numPhotos = 0;
        this.photos = new ArrayList<>();
    }

    /**
     * This method is a constructor for the Album object for making an album with search results.
     * @param name is the name to place on the album.
     * @param photos is a list of photos to be placed in the album.
     */
    public Album(String name, List<Photo> photos) {
        this.name = name.toLowerCase();
        this.numPhotos = photos.size();
        this.photos = photos;
    }

    /**
     * This method is a getter function for the album's name.
     * @return Returns the name of the album.
     */
    public String getName() {
        return name;
    }

    /**
     * THis method is a getter function for the number of photos in the album.
     * @return Returns the number of photos in the album.
     */
    public int getNumPhotos() {
        return numPhotos;
    }

    /**
     * This method is a getter function for the photos in the album.
     * @return Returns the photos in the album.
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * This method renames the album.
     * @param name is the new name to be given to the album.
     * @return Returns whether renaming was successful.
     */
    public boolean setName(String name) {
        if (name.length() != 0) {
            this.name = name.toLowerCase();
            return true;
        }
        return false;
    }

    /**
     * This method adds a photo to the album using a filepath.
     * @param filepath is the filepath where the photo is stored on the disk.
     * @return Returns whether adding the photo was successful.
     */
    public boolean addPhoto(String filepath) {
        File f = new File(filepath);
        if(f.exists() && !f.isDirectory()) {
            photos.add(new Photo(filepath));
            numPhotos++;
            return true;
        }
        return false;
    }

    /**
     * This method adds an existing Photo object to the album.
     * @param photo is the photo to add to the album.
     * @return Returns whether adding the photo was successful.
     */
    public boolean addPhoto(Photo photo) {
        if (photos.contains(photo)) {
            return false;
        }
        photos.add(photo);
        numPhotos++;
        return true;
    }

    /**
     * This method removes a photo from the album.
     * @param photo is the photo to remove from the album.
     */
    public void removePhoto(Photo photo) { photos.remove(photo); numPhotos--;}
}

package main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is the Tag object that stores the tags of a certain type on a photo and whether there can be more
 * than one tag of that type.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class Tag implements Serializable {

    /**
     * This field: serialVersionUID, generates a default id and is added to all methods that implement Serializable.
     */
    static final long serialVersionUID = 1L;

    /**
     * This field: canHaveMultiple, is whether the tag can contain multiple values or just one.
     */
    private boolean canHaveMultiple;

    /**
     * This field: tags, is the list of tags.
     */
    private List<String> tags;

    /**
     * This method is the constructor for the Tag object.
     * @param canHaveMultiple is whether the tag can contain multiple values or just one.
     */
    public Tag(boolean canHaveMultiple) {
        this.canHaveMultiple = canHaveMultiple;
        this.tags = new ArrayList<>();
    }

    /**
     * This method is a getter function for the tags in the tags list.
     * @return Returns the string of tags stored by the tag.
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * This method adds a tag to the tag list.
     * @param tag is the tag to be added.
     * @return whether adding the tag was successful.
     */
    boolean addTag(String tag) {
        if (!tags.isEmpty() && !canHaveMultiple) {
            return false;
        }
        if (tags.contains(tag)) {
            return false;
        }
        tags.add(tag.toLowerCase());
        return true;
    }

    /**
     * This method is a getter function for whether the tag can contain multiple values.
     * @return Whether the tag can hold multiple values.
     */
    public boolean isCanHaveMultiple() {
        return canHaveMultiple;
    }
}

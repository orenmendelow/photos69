package main;

import java.io.Serializable;
import java.util.*;

/**
 * This class is the user object which is stored by the application. It stores albums and all objects contained
 * within those albums.
 *
 * @author Oren Mendelow
 * @author John Wade
 */
public class User implements Serializable {
    /**
     * This field: serialVersionUID, generates a default id and is added to all methods that implement Serializable.
     */
    static final long serialVersionUID = 1L;

    /**
     * This field: name, the username of the user.
     */
    private String name;

    /**
     * This field, userAlbums, the user's albums.
     */
    private List<Album> albums;

    /**
     * This field: userPhotos, all of the user's photos, non-album specific.
     */
    private List<Photo> photos;

    /**
     * This field: tagType, is all tag types for photos in the user's library.
     */
    private Map<String, Tag> tagTypes;

    /**
     * This method is the constructor for the User object.
     * @param name is the username to assign to the user.
     */
    public User(String name) {
        this.name = name.toLowerCase();
        this.albums = new ArrayList<>();
        this.photos = new ArrayList<>();
        this.tagTypes = new HashMap<>();
        this.tagTypes.put("person", new Tag(true));
        this.tagTypes.put("location", new Tag(false));
    }

    /**
     * This method is a getter function for the user's tag types.
     * @return Returns the tagTypes map.
     */
    public Map<String, Tag> getTagTypes() {
        return tagTypes;
    }

    /**
     * This method is a getter function for the user's name.
     * @return Returns the user's name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * This method is a getter function for the user's albums.
     * @return Returns the user's albums.
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * This method retrieves and album by name.
     * @param name is the name of the album.
     * @return Returns the album with name if found, null if not found.
     */
    public Album getAlbum(String name) {
        for (Album album : albums) {
            if (album.getName().equals(name)) {
                return album;
            }
        }
        return null;
    }

    /**
     * This method is a getter function for the user's photos.
     * @return Returns the user's photos.
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * This method adds a tag type and will allow the user to add tags of this type to all photos in their library.
     * @param tagType is the type of tag to be added.
     * @param canHaveMultiple is whether this tag type can contain multiple tags at once.
     */
    public void addTagType(String tagType, boolean canHaveMultiple) {
        tagType = tagType.toLowerCase();
        this.tagTypes.put(tagType, new Tag(canHaveMultiple));
        for (Photo photo : photos) {
            photo.addTagType(tagType, canHaveMultiple);
        }
    }

    /**
     * This method adds an existing album to the user's library.
     * @param album is the album object to add to the user's library.
     */
    public void addAlbum(Album album) {
        albums.add(album);
    }

    /**
     * This method removes an album from the user's library.
     * @param albumName is the name of the album to remove.
     */
    public void removeAlbum(String albumName) {
        for (Album album : albums) {
            if (album.getName().equals(albumName)) {
                albums.remove(album);
                return;
            }
        }
    }

    /**
     * This method adds a new album to the user's library.
     * @param name is the name to assign to the new album.
     * @return Returns whether adding the album with specified name was successful.
     */
    public boolean addAlbum(String name) {
        for (Album album : albums) {
            if (album.getName().equals(name)) {
                return false;
            }
        }
        albums.add(new Album(name));
        return true;
    }

    /**
     * This method adds a new album to the user's library with specified photos to include in the new album.
     * @param name is the name to assign to the new album.
     * @param photos is the list of photos to place in the new album.
     * @return Returns whether adding the album with specified name and photos was successful.
     */
    public boolean addAlbum(String name, List<Photo> photos) {
        for (Album album : albums) {
            if (album.getName().equals(name)) {
                return false;
            }
        }
        albums.add(new Album(name, photos));
        return true;
    }

    /**
     * This method adds a photo to one of the user's albums.
     * @param album is the album in which to add the photo.
     * @param photo is the photo to add to the album.
     * @return Returns if adding photo to album was successful.
     */
    public boolean addPhoto(Album album, Photo photo) {
        if (!album.getPhotos().contains(photo)) {
            album.addPhoto(photo);
            if (!photos.contains(photo)) {
                photos.add(photo);
            }
            return true;
        }
        return false;
    }

    /**
     * This method returns a specified photo from a specified album.
     * @param album is the album from which to delete the photo.
     * @param photo is the photo to delete.
     */
    public void removePhoto(Album album, Photo photo) {
        album.removePhoto(photo);
        for (Album currAlbum : albums) {
            if (currAlbum.getPhotos().contains(photo)) {
                break;
            }
        }
        photos.remove(photo);
    }

    /**
     * This method adds a new photo to one of the user's albums.
     * @param album is the album in which to add the photo.
     * @param filepath is the filepath where the photo is location on the disk.
     */
    public void addPhoto(Album album, String filepath) {
        Photo newPhoto = new Photo(filepath);
        album.addPhoto(newPhoto);
        photos.add(newPhoto);
    }

    /**
     * This method searches for all photos within a date-time range.
     * @param from is the date-time from when to search.
     * @param to is the date-time until when to search.
     * @return Returns the results of the search.
     */
    public List<Photo> search(Date from, Date to) {
        List<Photo> searchResults = new ArrayList<>();
        for (Photo photo : photos) {
            if (from.compareTo(photo.getDate().getTime()) <= 0 && to.compareTo(photo.getDate().getTime()) >= 0) {
                searchResults.add(photo);
            }
        }
        return searchResults;
    }

    /**
     * This method searches for all photos that have a specific tag-value pair.
     * @param query is the tagType and value for which to search. For example, "person=sesh"
     * @return Returns the results of the search.
     */
    public List<Photo> search(String query) {
        // check if conjunctive or disjunctive
        query = query.toLowerCase();
        if (query.contains(" and ") | query.contains(" or ")) {
            return search(query.split(" "));
        }
        List<Photo> searchResults = new ArrayList<>();
        int splitIndex = query.indexOf('=');
        String tagType = query.substring(0,splitIndex);
        String tagValue = query.substring(splitIndex+1);
        if (getTagTypes().containsKey(tagType)) {
            for (Photo photo : photos) {
                for (String tag : photo.getTags(tagType)) {
                    if (tagValue.equals(tag)) {
                        searchResults.add(photo);
                    }
                }
            }
        }
        return searchResults;
    }

    /**
     * This method performs a conjunctive or disjunctive combination search of two tag-value pairs
     * @param query is the search to perform, the first and third values are the tag-value pairs to search for,
     *              and the middle value is whether to perform a conjunctive or disjunctive search.
     * @return Returns the results of the search.
     */
    public List<Photo> search(String[] query) {
        String query1 = query[0];
        String operand = query[1];
        String query2 = query[2];
        List<Photo> searchResults = new ArrayList<>();
        List<Photo> query1Results = search(query1);
        List<Photo> query2Results = search(query2);
        if (operand.equals("or")) {
            searchResults.addAll(query1Results);
            for (Photo photo : query2Results) {
                if (!searchResults.contains(photo)) {
                    searchResults.add(photo);
                }
            }
        }
        else if (operand.equals("and")) {
            for (Photo photo : query1Results) {
                if (query2Results.contains(photo)) {
                    searchResults.add(photo);
                }
            }
        }
        else {
            // error
        }
        return searchResults;
    }
}
